<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0"/>
    <meta name="description" content="Pusat Bahasa Isyarat Indonesia">
    <meta name="keywords" content="Pusat Bahasa Isyarat Indonesia">
    <title>PUSBISINDO | Pusat Bahasa Isyarat Indonesia</title>
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon180x180.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon16x16.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon32x32.png')}}">
    
    <!-- CSS -->
    @yield('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/front.css?v.4') }}" rel="stylesheet"/>

</head>
<body class="bg-main" style="background-image: url('{{asset('images/maintenance.png?v.1')}}');') no-repeat center;">
	<div class="maintenance">
		<div class="abs-maintenance">
			<ul>
				<li>Beranda</li>
				<li>Under Maintenance</li>
			</ul>
		</div>
		<div class="tbl">
			<div class="cell">
				<div class="container">
					<div class="logo"><img src="{{asset('images/logo-maintenance.png?v.1')}}" alt="" title=""/></div>
					<div id="countdown">
						<ul>
							<li><span id="days"></span>days</li>
							<li><span id="hours"></span>Hours</li>
							<li><span>:</span></li>
							<li><span id="minutes"></span>Minutes</li>
							<li><span>:</span></li>
							<li><span id="seconds"></span>Seconds</li>
						</ul>
					</div>
					<div class="txt">We’re currently working on our new website! </div>
					<div class="txt2">We’ll be launched soon!</div>
				</div>
			</div>
		</div>
	</div>

<style type="text/css">
	body {
		padding-top:  0;
	}
</style>

<script type="text/javascript" src="{{ asset('jquery-3.5.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
$(function() {
	const second = 1000,
	minute = second * 60,
	hour = minute * 60,
	day = hour * 24;

	let birthday = "Jan 1, 2021 12:00:00",
	countDown = new Date(birthday).getTime(),
	x = setInterval(function() {    
		let now = new Date().getTime(),
		distance = countDown - now;
		document.getElementById("days").innerText = Math.floor(distance / (day)),
		document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
		document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
		document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
	}, 0)
});
</script>

</body>
</html>
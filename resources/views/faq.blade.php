@extends('layout')

@section('content')
	<div class="css-faq">
		<div class="bg-faq" style="background-image: url('{{asset('images/banner.jpg?v.1')}}');">
			<div class="tbl">
				<div class="cell">
	    			<div class="container">
	    				<div class="t-faq">Halo! Apa yang anda ingin ketahui?</div>
	    				<div class="bdy-faq">
	    					<p>Temukan informasi yang ingin anda ketahui mengenai dunia Tuli dan Bahasa Isyarat indonesia</p>
	    				</div>
	    				<div class="input-faq">
	    					<input type="text" class="form-control" placeholder="Cari Topik..." />
	    					<button type="submit"><img src="{{asset('images/search.png?v.1')}}" alt="" title=""/></button>
	    				</div>
	    				<div class="text-faq">Contoh: Anak Tuli, Bahasa Isyarat, Bayi. </div>
					</div>
				</div>
			</div>
		</div>

		<div class="pad-program">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="title-faq">Kategori</div>
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="faq1-tab" data-toggle="tab" href="#faq1" role="tab" aria-controls="faq1" aria-selected="true">Seputar Dunia Tuli</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="faq2-tab" data-toggle="tab" href="#faq2" role="tab" aria-controls="faq2" aria-selected="false">Dunia PUSBISINDO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="faq3-tab" data-toggle="tab" href="#faq3" role="tab" aria-controls="faq3" aria-selected="false">Kiat BerBISINDO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="faq4-tab" data-toggle="tab" href="#faq4" role="tab" aria-controls="faq4" aria-selected="false">Sejarah PUSBISINDO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="faq5-tab" data-toggle="tab" href="#faq5" role="tab" aria-controls="faq5" aria-selected="false">Lain-lain</a>
							</li>
						</ul>
					</div>
					<div class="col-md-9">
						<div class="title-faq">Pertanyaan yang paling sering diajukan</div>
						<div class="tab-content">
							<div class="tab-pane fade show active" id="faq1" role="tabpanel" aria-labelledby="faq1-tab">
								<ul class="accordionFaq" id="accordionFaq1">
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse1-1" aria-expanded="false" aria-controls="collapse1-1"><i class="fas fa-angle-right"></i> Siapa saja yang bisa belajar BISINDO?</div>
										<div id="collapse1-1" class="collapse" data-parent="#accordionFaq1">
											<div class="bdy-answer">
												<p>semua orang termasuk anak dengar, orang tua, anak Tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse1-2" aria-expanded="false" aria-controls="collapse1-2"><i class="fas fa-angle-right"></i> Apa itu keluarga Tuli?</div>
										<div id="collapse1-2" class="collapse" data-parent="#accordionFaq1">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse1-3" aria-expanded="false" aria-controls="collapse1-3"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau dengar bisa belajar bahasa isyarat? </div>
										<div id="collapse1-3" class="collapse" data-parent="#accordionFaq1">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse1-4" aria-expanded="false" aria-controls="collapse1-4"><i class="fas fa-angle-right"></i> Apakah ada metode untuk mengajar BISINDO ke anak Tuli?</div>
										<div id="collapse1-4" class="collapse" data-parent="#accordionFaq1">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse1-5" aria-expanded="false" aria-controls="collapse1-5"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau Lalu, bagaimana dengan anak dengar yang dilahirkan oleh orang tua Tuli juga? Dapatkah mereka berbahasa isyarat juga?</div>
										<div id="collapse1-5" class="collapse" data-parent="#accordionFaq1">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane fade" id="faq2" role="tabpanel" aria-labelledby="faq2-tab">
								<ul class="accordionFaq" id="accordionFaq2">
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse2-1" aria-expanded="false" aria-controls="collapse2-1"><i class="fas fa-angle-right"></i> Siapa saja yang bisa belajar BISINDO?</div>
										<div id="collapse2-1" class="collapse" data-parent="#accordionFaq2">
											<div class="bdy-answer">
												<p>semua orang termasuk anak dengar, orang tua, anak Tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse2-2" aria-expanded="false" aria-controls="collapse2-2"><i class="fas fa-angle-right"></i> Apa itu keluarga Tuli?</div>
										<div id="collapse2-2" class="collapse" data-parent="#accordionFaq2">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse2-3" aria-expanded="false" aria-controls="collapse2-3"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau dengar bisa belajar bahasa isyarat? </div>
										<div id="collapse2-3" class="collapse" data-parent="#accordionFaq2">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse2-4" aria-expanded="false" aria-controls="collapse2-4"><i class="fas fa-angle-right"></i> Apakah ada metode untuk mengajar BISINDO ke anak Tuli?</div>
										<div id="collapse2-4" class="collapse" data-parent="#accordionFaq2">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse2-5" aria-expanded="false" aria-controls="collapse2-5"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau Lalu, bagaimana dengan anak dengar yang dilahirkan oleh orang tua Tuli juga? Dapatkah mereka berbahasa isyarat juga?</div>
										<div id="collapse2-5" class="collapse" data-parent="#accordionFaq2">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane fade" id="faq3" role="tabpanel" aria-labelledby="faq3-tab">
								<ul class="accordionFaq" id="accordionFaq3">
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse3-1" aria-expanded="false" aria-controls="collapse3-1"><i class="fas fa-angle-right"></i> Siapa saja yang bisa belajar BISINDO?</div>
										<div id="collapse3-1" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>semua orang termasuk anak dengar, orang tua, anak Tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse3-2" aria-expanded="false" aria-controls="collapse3-2"><i class="fas fa-angle-right"></i> Apa itu keluarga Tuli?</div>
										<div id="collapse3-2" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse3-3" aria-expanded="false" aria-controls="collapse3-3"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau dengar bisa belajar bahasa isyarat? </div>
										<div id="collapse3-3" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse3-4" aria-expanded="false" aria-controls="collapse3-4"><i class="fas fa-angle-right"></i> Apakah ada metode untuk mengajar BISINDO ke anak Tuli?</div>
										<div id="collapse3-4" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse3-5" aria-expanded="false" aria-controls="collapse3-5"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau Lalu, bagaimana dengan anak dengar yang dilahirkan oleh orang tua Tuli juga? Dapatkah mereka berbahasa isyarat juga?</div>
										<div id="collapse3-5" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane fade" id="faq4" role="tabpanel" aria-labelledby="faq4-tab">
								<ul class="accordionFaq" id="accordionFaq3">
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse4-1" aria-expanded="false" aria-controls="collapse4-1"><i class="fas fa-angle-right"></i> Siapa saja yang bisa belajar BISINDO?</div>
										<div id="collapse4-1" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>semua orang termasuk anak dengar, orang tua, anak Tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse4-2" aria-expanded="false" aria-controls="collapse4-2"><i class="fas fa-angle-right"></i> Apa itu keluarga Tuli?</div>
										<div id="collapse4-2" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse4-3" aria-expanded="false" aria-controls="collapse4-3"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau dengar bisa belajar bahasa isyarat? </div>
										<div id="collapse4-3" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse4-4" aria-expanded="false" aria-controls="collapse4-4"><i class="fas fa-angle-right"></i> Apakah ada metode untuk mengajar BISINDO ke anak Tuli?</div>
										<div id="collapse4-4" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse4-5" aria-expanded="false" aria-controls="collapse4-5"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau Lalu, bagaimana dengan anak dengar yang dilahirkan oleh orang tua Tuli juga? Dapatkah mereka berbahasa isyarat juga?</div>
										<div id="collapse4-5" class="collapse" data-parent="#accordionFaq3">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane fade" id="faq5" role="tabpanel" aria-labelledby="faq5-tab">
								<ul class="accordionFaq" id="accordionFaq5">
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse5-1" aria-expanded="false" aria-controls="collapse5-1"><i class="fas fa-angle-right"></i> Siapa saja yang bisa belajar BISINDO?</div>
										<div id="collapse5-1" class="collapse" data-parent="#accordionFaq5">
											<div class="bdy-answer">
												<p>semua orang termasuk anak dengar, orang tua, anak Tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse5-2" aria-expanded="false" aria-controls="collapse5-2"><i class="fas fa-angle-right"></i> Apa itu keluarga Tuli?</div>
										<div id="collapse5-2" class="collapse" data-parent="#accordionFaq5">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse5-3" aria-expanded="false" aria-controls="collapse5-3"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau dengar bisa belajar bahasa isyarat? </div>
										<div id="collapse5-3" class="collapse" data-parent="#accordionFaq5">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse5-4" aria-expanded="false" aria-controls="collapse5-4"><i class="fas fa-angle-right"></i> Apakah ada metode untuk mengajar BISINDO ke anak Tuli?</div>
										<div id="collapse5-4" class="collapse" data-parent="#accordionFaq5">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
									<li>
										<div class="in collapsed" data-toggle="collapse" data-target="#collapse5-5" aria-expanded="false" aria-controls="collapse5-5"><i class="fas fa-angle-right"></i> Apakah anak Tuli atau Lalu, bagaimana dengan anak dengar yang dilahirkan oleh orang tua Tuli juga? Dapatkah mereka berbahasa isyarat juga?</div>
										<div id="collapse5-5" class="collapse" data-parent="#accordionFaq5">
											<div class="bdy-answer">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')

<script type="text/javascript">
    $(function() {
    	$('.main-menu nav a').removeClass('active');
    	
    	$('.nav-faq').addClass('active');

    	$(".collapse.show").each(function(){
	        $(this).prev("ul.accordionFaq li .in").find(".fas").addClass("fa-angle-down").removeClass("fa-angle-right");
	    });
	    
	    $(".collapse").on('show.bs.collapse', function(){
	        $(this).prev("ul.accordionFaq li .in").find(".fas").removeClass("fa-angle-right").addClass("fa-angle-down");
	    }).on('hide.bs.collapse', function(){
	        $(this).prev("ul.accordionFaq li .in").find(".fas").removeClass("fa-angle-down").addClass("fa-angle-right");
	    });
    });
</script>
@endsection
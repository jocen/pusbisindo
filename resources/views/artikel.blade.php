@extends('layout')

@section('css')
<link href="{{ asset('slick/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('slick/slick-theme.css') }}" rel="stylesheet"/>
@endsection

@section('content')
	<div class="css-artikel">
		<div class="slider-banner">
			<div class="item" style="background: url('{{asset('images/bg-artikel.jpg?v.1')}}') no-repeat top;">
				<div class="tbl">
					<div class="cell">
		    			<div class="container">
		    				<div class="row">
		    					<div class="col-md-6 col-lg-5 col-xl-4">
				    				<div class="t-banner">Etika Berkomunikasi dengan Tuli</div>
				    				<div class="bdy-banner">
				    					<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat dengan metode kelas yang efisien dan menyenangkan.</p>
				    				</div>
				    				<div class="link">
				    					<a href="{{ URL::to('/artikel-detail') }}"><button type="button" class="hvr-button">Lanjut Baca</button></a>
				    				</div>
				    			</div>
				    		</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background: url('{{asset('images/bg-artikel.jpg?v.1')}}') no-repeat top;">
				<div class="tbl">
					<div class="cell">
		    			<div class="container">
		    				<div class="row">
		    					<div class="col-md-6 col-lg-5 col-xl-4">
				    				<div class="t-banner">Etika Berkomunikasi dengan Tuli</div>
				    				<div class="bdy-banner">
				    					<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat dengan metode kelas yang efisien dan menyenangkan.</p>
				    				</div>
				    				<div class="link">
				    					<a href="{{ URL::to('/artikel-detail') }}"><button type="button" class="hvr-button">Lanjut Baca</button></a>
				    				</div>
				    			</div>
				    		</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background: url('{{asset('images/bg-artikel.jpg?v.1')}}') no-repeat top;">
				<div class="tbl">
					<div class="cell">
		    			<div class="container">
		    				<div class="row">
		    					<div class="col-md-6 col-lg-5 col-xl-4">
				    				<div class="t-banner">Etika Berkomunikasi dengan Tuli</div>
				    				<div class="bdy-banner">
				    					<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat dengan metode kelas yang efisien dan menyenangkan.</p>
				    				</div>
				    				<div class="link">
				    					<a href="{{ URL::to('/artikel-detail') }}"><button type="button" class="hvr-button">Lanjut Baca</button></a>
				    				</div>
				    			</div>
				    		</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pad-program">
			<div class="container">
				<div class="bdy-program mb10">
	    			<p>Temukan apa yang ingin kamu ketahui:</p>
	    		</div>
	    		<div class="text-center">
		    		<div class="input-artikel">
						<input type="text" class="form-control" placeholder="Cari..." />
						<button type="submit"><img src="{{asset('images/search2.png?v.1')}}" alt="" title=""/></button>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="box-artikel">
							<div class="row">
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
								<div class="col-md-4">
									<div class="item-artikel">
		                                <a href="{{ URL::to('/artikel-detail') }}">
		                                    <div class="in-artikel" style="background:url('{{asset('images/artikel.jpg?v.1')}}')">
		                    					<div class="hvr-artikel">
		                                            <div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan</p>
		                                            </div>
		                                        </div>
							                </div>
		            					</a>
		        					</div>
								</div>
							</div>
							<ul class="l-pag">
								<li><a href="#"><</a></li>
								<li><a href="#" class="active">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">></a></li>
							</ul>
							<div class="t-artikel">Artikel Populer</div>
							<div class="row">
								<div class="col-md-6">
									<div class="item-artikel2">
										<a href="{{ URL::to('/artikel-detail') }}">
											<div class="tbl">
												<div class="cell img-artikel">
													<img src="{{asset('images/artikel.jpg?v.1')}}" alt="" title=""/>
												</div>
												<div class="cell">
													<div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan...</p>
		                                            </div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="col-md-6">
									<div class="item-artikel2">
										<a href="{{ URL::to('/artikel-detail') }}">
											<div class="tbl">
												<div class="cell img-artikel">
													<img src="{{asset('images/artikel.jpg?v.1')}}" alt="" title=""/>
												</div>
												<div class="cell">
													<div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
		                                            <div class="bdy">
		                                            	<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat..</p>
		                                            </div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>

<script type="text/javascript">
	function widthMaxbdy(){
		if ($(window).width() > 1024) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 120) + '...');
		    });
		}
		else if ($(window).width() > 992) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 100) + '...');
		    });
		}
		else if ($(window).width() > 767) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 80) + '...');
		    });
		}
	}

	function widthMaxbdy2(){
		if ($(window).width() > 1024) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 140) + '...');
		    });
		}
		else if ($(window).width() > 992) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 80) + '...');
		    });
		}
		else if ($(window).width() > 767) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 60) + '...');
		    });
		}
		else if ($(window).width() < 766) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 40) + '...');
		    });
		}
	}
    $(function() {
    	$('.nav-artikel').addClass('active');

    	$('.slider-banner').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
            autoplay: true,
			fade: false,
			adaptiveHeight: true
		});

	    widthMaxbdy();
	    widthMaxbdy2();
		
		$(window).on('resize', function(){
			widthMaxbdy();
			widthMaxbdy2();
		});
    });
</script>
@endsection
@extends('layout')

@section('css')
<link href="{{ asset('slick/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('slick/slick-theme.css') }}" rel="stylesheet"/>
@endsection

@section('content')
	<div class="bg-tentang custom">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<div class="img">
		    			<img src="{{asset('images/team.png')}}" alt="" title=""/>
		    		</div>
				</div>
				<div class="col-lg-7">
					<div class="pl30">
						<div class="t">Latar Belakang berdirinya PUSBISINDO</div>
						<div class="bdy">
							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan. Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
						</div>
	    			</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pad-program" id="timeline">
		<div class="container">
			<div class="t-program">Timeline</div>
    		<div class="bdy-program">
    			<p>Bisa berupa perjalanan berawal dari asal mula bisindo, hingga berdirinya pusbisindo bahkan sampai sekarang,</p>
                <p>bisa juga dilampirkan apa saja pencapaian pusbisindo seiring perjalanan waktu hingga sekarang.</p>
    		</div>
    		<ul class="timeline">
    			<li>
					<div class="direction-l sr-left-td1">
						<div class="year">Tahun 1933</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
                                <p>Gedung Sekolah Luar Biasa Cicendo Bandung diresmikan pemakainnya untuk sekolah dan asrama anak-anak tuli bisu Indonesia pada tanggal 18 Desember 1933. Gedung ini dibangun atas sumbangan dan prakarsa Tn. Kar. Bosscha. Berdasar SK GUbernur Kepala Daerah Tk I Jawa Barat no 41.8/Kep 576-HUK/1988, tanggal 21 April 1988, gedung SLB Cicendo dikelola oleh P3ATR (Perkumpulan Penyelenggara Pengajaran Bagi Anak-anak Tuna Rungu Indonesia). Dengan demikian, sampai sekarang gedung ini masih difunsikan sebagai SLB.</p>
    							<p>Gedung Sekolah Luar Biasa Cicendo Bandung diresmikan pemakainnya untuk sekolah dan asrama anak-anak tuli bisu Indonesia pada tanggal 18 Desember 1933. Gedung ini dibangun atas sumbangan dan prakarsa Tn. Kar. Bosscha. Berdasar SK GUbernur Kepala Daerah Tk I Jawa Barat no 41.8/Kep 576-HUK/1988, tanggal 21 April 1988, gedung SLB Cicendo dikelola oleh P3ATR (Perkumpulan Penyelenggara Pengajaran Bagi Anak-anak Tuna Rungu Indonesia). Dengan demikian, sampai sekarang gedung ini masih difunsikan sebagai SLB.</p>
    						</div>
                        </div>
					</div>
				</li>
				<li>
					<div class="direction-r sr-right-td1">
						<div class="year">Tahun 1938</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
    							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
    						</div>
                        </div>
					</div>
				</li>
				<li>
					<div class="direction-l sr-left-td1">
						<div class="year">Tahun 1933</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
    							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
    						</div>
                        </div>
					</div>
				</li>
				<li>
					<div class="direction-r sr-right-td1">
						<div class="year">Tahun 1938</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
    							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
    						</div>
                        </div>
					</div>
				</li>
				<li>
					<div class="direction-l sr-left-td1">
						<div class="year">Tahun 1933</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
    							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
    						</div>
                        </div>
					</div>
				</li>
				<li>
					<div class="direction-r sr-right-td1">
						<div class="year">Tahun 1938</div>
						<div class="pos-rel">
							<div class="clearfix">
								<div class="img"><img src="{{asset('images/timeline.jpg')}}" alt="" title=""/></div>
							</div>
							<div class="bdr"></div>
						</div>
                        <div class="clearfix">
    						<div class="desc">
    							<p>Kami memberikan pengajaran bahasa isyarat  dengan metode kelas yang efisien dan menyenangkan.</p>
    						</div>
                        </div>
					</div>
				</li>
    		</ul>
		</div>
	</div>

	<div class="bg-video">
		<div class="img-video"><img src="{{asset('images/background-team.png')}}" alt="" title=""/></div>
		<div class="abs">
			<div class="container">
				<iframe src="https://www.youtube.com/embed/UukZHJiFUdU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		<div class="abs-name">
			<div class="t1">Laura Lesmana Wijaya</div>
			<div class="t2">Ketua Pusat Bahasa Isyarat Indonesia</div>
		</div>
	</div>
    <div class="resp-video">
        <iframe src="https://www.youtube.com/embed/UukZHJiFUdU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

	<div class="pad-program" id="kepengurusan-pusat">
		<div class="container">
			<div class="t-program">STRUKTUR KEPENGURUSAN PUSAT PUSBISINDO</div>
    		<div class="bdy-program">
    			<p>PUSBISINDO Pusat berlokasi di DKI Jakarta dan dijalankan oleh tim yang beranggotakan 5 (lima) orang Tuli.</p>
    			<p>Setiap pengurus merupakan orang-orang yang profesional di bidangnya. Penasaran? Mari kita intip profil singkat mereka di bawah ini!</p>
    		</div>
    		<div class="slider-team">
    			<div class="item">
    				<div class="row justify-content-center">
    					<div class="col-lg-9">
    						<div class="row">
    							<div class="col-md-5">
    								<div class="pr30">
	    								<div class="img-team"><img src="{{asset('images/laura.jpg')}}" alt="" title=""/></div>
	    							</div>
    							</div>
    							<div class="col-md-7">
    								<div class="nm-team">Laura Lesmana Wijaya</div>
    								<div class="jab-team">Ketua Pusat Bahasa Isyarat Indonesia</div>
    								<div class="desc-team">
    									<p>Saya lahir dari keluarga Tuli dan dibesarkan di dua dunia sehingga saya memperoleh dua bahasa dan dua budaya, Tuli dan dengar. Saya berkuliah di The Chinese University of Hong Kong dari Diploma hingga Magister jurusan Linguistik Bahasa Isyarat. Banyak pengalaman saya di dunia leksikografi dan linguistik bahasa isyarat dan juga pengajaran bahasa isyarat. Selain itu, kegiatan saya juga memperdayakan dan mendorong teman-teman Tuli untuk mendapatkan hak yang setara dengan lainnya baik di tingkat nasional dan internasional.</p>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="item">
    				<div class="row justify-content-center">
    					<div class="col-lg-9">
    						<div class="row">
    							<div class="col-md-5">
    								<div class="pr30">
	    								<div class="img-team"><img src="{{asset('images/laura.jpg')}}" alt="" title=""/></div>
	    							</div>
    							</div>
    							<div class="col-md-7">
    								<div class="nm-team">Laura Lesmana Wijaya</div>
    								<div class="jab-team">Ketua Pusat Bahasa Isyarat Indonesia</div>
    								<div class="desc-team">
    									<p>Saya lahir dari keluarga Tuli dan dibesarkan di dua dunia sehingga saya memperoleh dua bahasa dan dua budaya, Tuli dan dengar. Saya berkuliah di The Chinese University of Hong Kong dari Diploma hingga Magister jurusan Linguistik Bahasa Isyarat. Banyak pengalaman saya di dunia leksikografi dan linguistik bahasa isyarat dan juga pengajaran bahasa isyarat. Selain itu, kegiatan saya juga memperdayakan dan mendorong teman-teman Tuli untuk mendapatkan hak yang setara dengan lainnya baik di tingkat nasional dan internasional.</p>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="bg-cabang"  id="kepengurusan-cabang" style="background: url('{{asset('images/bg-cabang.jpg')}}') no-repeat center;">
    	<div class="pad-program">
    		<div class="bg-elips"><img src="{{asset('images/cabang2.png')}}" alt="" title="" /></div>
    		<div class="abs-elips">
	    		<div class="container">
	    			<div class="t-program">PENGURUS PUSBISINDO CABANG</div>
		    		<div class="bdy-program mb0">
		    			<p>PUSBISINDO memiliki cabang yang telah tersebar di 5 (lima) provinsi.</p>
		    			<p>Setiap cabang mempunyai tim pengurus yang menjalankan kelas BISINDO di daerah tersebut.</p>
		    		</div>
		    	</div>
		    </div>
	    </div>
    	<div class="container">
    		<div class="slider-cabang">
    			<div class="item">
    				<div class="t-cabang">DIY YOGYAKARTA</div>
    				<ul class="l-cabang three">
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/wahyu.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Wahyu Triwibowo</div>
    						<div class="jab-cabang">Humas</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/davi.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Muhammad Dafi Muchlisin</div>
    						<div class="jab-cabang">Koordinator</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/nazwa.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Nazwa Sharfina E.</div>
    						<div class="jab-cabang">Bendahara</div>
    					</li>
    				</ul>
    				<ul class="l-cabang five">
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/diki.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Muhammad Diki Prasetyo</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/riski.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Riski Purna Adi</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/guruh.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Guruh Hizbullah Alim</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/anggi.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Anggi Toberti</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/indra.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Indra Kurmala</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    				</ul>
    			</div>
    			<div class="item">
    				<div class="t-cabang">Jakarta</div>
    				<ul class="l-cabang three">
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/wahyu.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Wahyu Triwibowo</div>
    						<div class="jab-cabang">Humas</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/davi.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Muhammad Dafi Muchlisin</div>
    						<div class="jab-cabang">Koordinator</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/nazwa.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Nazwa Sharfina E.</div>
    						<div class="jab-cabang">Bendahara</div>
    					</li>
    				</ul>
    				<ul class="l-cabang five">
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/diki.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Muhammad Diki Prasetyo</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/riski.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Riski Purna Adi</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/guruh.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Guruh Hizbullah Alim</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/anggi.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Anggi Toberti</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    					<li>
    						<div class="img-cabang"><img src="{{asset('images/indra.png')}}" alt="" title=""/></div>
    						<div class="nm-cabang">Indra Kurmala</div>
    						<div class="jab-cabang">Guru</div>
    					</li>
    				</ul>
    			</div>
    		</div>
    	</div>
    </div>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<script type="text/javascript">
    $(function() {
    	$('.slider-team').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
            autoplay: false,
			fade: false,
			adaptiveHeight: true
		});

		$('.slider-cabang').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
            autoplay: false,
			fade: false,
			adaptiveHeight: true
		});

		$('.nav-tentang-kami').addClass('active');
    });
</script>
@endsection
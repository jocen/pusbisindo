@extends('layout')

@section('css')
<link href="{{ asset('slick/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('slick/slick-theme.css') }}" rel="stylesheet"/>
@endsection

@section('content')
	<section id="beranda" data-anchor="beranda">
    	<div class="slider-banner">
    		<div class="item">
    			<div class="bg"><img src="{{ asset('images/banner.jpg') }}"/></div>
				<div class="tbl">
					<div class="cell">
		    			<div class="container">
		    				<div class="clearfix">
			    				<div class="w-text">
				    				<div class="t-banner">
				    					<div>Ingin Fasih Berbahasa Isyarat?</div>
				    					<div>Memiliki kendala berkomunikasi dengan anak Tuli?</div>
				    				</div>
				    				<div class="bdy-banner">
				    					<p>
				    						<span style="font-weight: 400;">Kami menyediakan pengajaran Bahasa Isyarat Indonesia (BISINDO) dengan metode</span> <em><span style="font-weight: 400;">the Natural Approach, the Communicative Language Teaching </span></em><span style="font-weight: 400;">dan <em>T</em></span><em><span style="font-weight: 400;">ask-based Language Teaching </span></em><span style="font-weight: 400;">yang menyenangkan dan interaktif.</span>
				    					</p>
				    				</div>
				    				<div class="link">
				    					<a class="click-kontak-kami"><button type="button" class="hvr-button">Kontak Kami</button></a>
				    				</div>
				    			</div>
	    					</div>
	    				</div>
    				</div>
    			</div>
    		</div>
    		<div class="item">
    			<div class="bg"><img src="{{ asset('images/banner.jpg') }}"/></div>
				<div class="tbl">
					<div class="cell">
		    			<div class="container">
		    				<div class="clearfix">
		    					<div class="w-text right">
				    				<div class="t-banner">
				    					<div>Ingin Fasih Berbahasa Isyarat?</div>
				    					<div>Memiliki kendala berkomunikasi dengan anak Tuli?</div>
				    				</div>
				    				<div class="bdy-banner">
				    					<p>
				    						<span style="font-weight: 400;">Kami menyediakan pengajaran Bahasa Isyarat Indonesia (BISINDO) dengan metode</span> <em><span style="font-weight: 400;">the Natural Approach, the Communicative Language Teaching </span></em><span style="font-weight: 400;">dan <em>T</em></span><em><span style="font-weight: 400;">ask-based Language Teaching </span></em><span style="font-weight: 400;">yang menyenangkan dan interaktif.</span>
				    					</p>
				    				</div>
				    				<div class="link">
				    					<a href="{{ URL::to('/') }}"><button type="button" class="hvr-button">Kontak Kami</button></a>
				    				</div>
				    			</div>
				    		</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section id="program" data-anchor="program">
    	<div class="pad-program">
    		<div class="container">
	    		<div class="t-program">Program Kelas Bahasa Isyarat PUSBISINDO</div>
	    		<div class="bdy-program">
	    			<p>Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda</p>
	    			<p>Semua program kelas didampingi para guru Tuli yang berpengalaman :</p>
	    		</div>
	    		<div class="slider-program">
	    			<div class="item">
	    				<div class="row">
	    					<div class="col-md-6 my-auto">
	    						<div class="img-program">
	    							<img src="{{asset('images/program.jpg')}}" alt="" title=""/>
	    						</div>
	    					</div>
	    					<div class="col-md-6 my-auto">
	    						<div class="pl30">
	    						<div class="nm-program">Kelas Reguler</div>
		    						<ul class="l-program">
		    							<li>1 - 2x meeting per minggu sesuai jadwal yang sudah ditentukan oleh PUSBISINDO</li>
		    							<li>Setiap kelas terdiri dari 15-20 orang per kelas</li>
		    							<li>Guru Tuli berpengalaman dan interaktif</li>
		    							<li>Mendapat sertifikat kelulusan sesuai level yang diambil.</li>
		    						</ul>
		    					</div>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="item">
	    				<div class="row">
	    					<div class="col-md-6 my-auto">
	    						<div class="img-program">
	    							<img src="{{asset('images/program.jpg')}}" alt="" title=""/>
	    						</div>
	    					</div>
	    					<div class="col-md-6 my-auto">
	    						<div class="pl30">
	    						<div class="nm-program">Kelas Privat</div>
		    						<ul class="l-program">
		    							<li>1 - 2x meeting per minggu sesuai jadwal yang sudah ditentukan oleh PUSBISINDO</li>
		    							<li>Setiap kelas terdiri dari 15-20 orang per kelas</li>
		    							<li>Guru Tuli berpengalaman dan interaktif</li>
		    							<li>Mendapat sertifikat kelulusan sesuai level yang diambil.</li>
		    						</ul>
		    					</div>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="text-center">
					<a class="click-kontak-kami inline-block"><button type="button" class="hvr-button red">Untuk info lebih detail, hubungi kami</button></a>
				</div>
	    	</div>
    	</div>
    	<div class="pad-program pt40">
    		<div class="container">
	    		<div class="t-program">Mengapa perlu belajar BISINDO?</div>
	    		<div class="bdy-program">
	    			<p>Bahasa Isyarat Indonesia (BISINDO) tidak terbatas hanya untuk Tuli tetapi juga untuk semua orang.</p>
	    			<p>Selain untuk mendukungnya komunikasi yang inklusif, mempelajari BISINDO juga mempunyai banyak manfaat.</p>
	    			<p>Apa saja? Mari kita simak di bawah ini! </p>
	    		</div>
	    		<div class="item-program">
		    		<div class="row">
		    			<div class="col-md-6 col-lg-3 sr-up-td1">
		    				<div class="in-program">
		    					<div class="img"><img src="{{asset('images/program1.png')}}" alt="" title=""/></div>
		    					<div class="nm">Memperkaya Ekspresi</div>
		    					<div class="desc">
		    						<p>BISINDO adalah komunikasi visual, tentu saja ekspresi berperan besar dalam menghidupkan suasana percakapan. Dengan mempelajari BISINDO dapat melatih dan memperkaya ekspresi Anda.</p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-md-6 col-lg-3 sr-up-td2">
		    				<div class="in-program">
		    					<div class="img"><img src="{{asset('images/program2.png')}}" alt="" title=""/></div>
		    					<div class="nm">Otak Kiri-Kanan Seimbang</div>
		    					<div class="desc">
		    						<p>Percaya atau tidak, Bahasa Isyarat dapat membantu keseimbangan perkembangan otak kiri dan kanan serta meningkatkan kecerdasan!</p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-md-6 col-lg-3 sr-up-td3">
		    				<div class="in-program">
		    					<div class="img"><img src="{{asset('images/program3.png')}}" alt="" title=""/></div>
		    					<div class="nm">Jaringan Lebih Luas</div>
		    					<div class="desc">
		    						<p>Anda akan lebih mudah terhubung dengan komunitas Tuli dan mempunyai banyak teman-teman Tuli. Selain itu, Anda berkesempatan menjadi Juru Bahasa Isyarat (JBI).</p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-md-6 col-lg-3 sr-up-td4">
		    				<div class="in-program">
		    					<div class="img"><img src="{{asset('images/program4.png')}}" alt="" title=""/></div>
		    					<div class="nm">Komunikasi tanpa Halaman</div>
		    					<div class="desc">
		    						<p>Dengan BISINDO, komunikasi tetap dapat dilakukan dalam berbagai situasi yang tidak mendukung sekalipun! Jarak terlalu jauh? Suasana terlalu bising? Bahkan di dalam air? Semua dapat diatasi dengan BISINDO!</p>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>
    </section>

    <section id="area-kelas" data-anchor="area-kelas">
    	<div class="pad-program pt40">
    		<div class="container">
	    		<div class="t-program">Area Kelas Pusbisindo Cabang</div>
	    		<div class="row justify-content-center">
	    			<div class="col-lg-10">
			    		<div class="bdy-program mb20">
			    			<p>PUSBISINDO mempunyai cabang yang tersebar di 5 (lima) Provinsi. Setiap cabang sudah membuka kelas Bahasa Isyarat di daerah masing-masing. Bagi Anda yang berdomisili di daerah yang sudah terdapat cabang PUSBISINDO, dapat langsung menghubungi kontak cabang yang tertera di bawah ini.</p>
			    		</div>
			    	</div>
			    </div>
	    	</div>
    		<!-- <div class="img-map">
    			<img src="{{asset('images/map.jpg')}}" alt="" title="" id="collapseBanten" class="open"/>
    			<img src="{{asset('images/map.jpg')}}" alt="" title="" id="collapseJakarta"/>
    			<img src="{{asset('images/map.jpg')}}" alt="" title="" id="collapseJawaBarat"/>
    			<img src="{{asset('images/map.jpg')}}" alt="" title="" id="collapseJawaTengah"/>
    			<img src="{{asset('images/map.jpg')}}" alt="" title="" id="collapseYogyakarta"/>
    		</div> -->
    		<!-- <div class="container">
    			<ul class="accordion accordionArea" id="accordionArea">
					<li>
						<div class="in collapsed" data-toggle="collapse" data-target="#collapseBanten" aria-expanded="false" aria-controls="collapseBanten"><i class="fas fa-angle-right"></i> Banten</div>
						<div id="collapseBanten" class="collapse" data-parent="#accordionArea">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li>
						<div class="in collapsed" data-toggle="collapse" data-target="#collapseJakarta" aria-expanded="false" aria-controls="collapseJakarta"><i class="fas fa-angle-right"></i> DKI Jakarta</div>
						<div id="collapseJakarta" class="collapse" data-parent="#accordionArea">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li>
						<div class="in collapsed" data-toggle="collapse" data-target="#collapseJawaBarat" aria-expanded="false" aria-controls="collapseJawaBarat"><i class="fas fa-angle-right"></i> Jawa Barat</div>
						<div id="collapseJawaBarat" class="collapse" data-parent="#accordionArea">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li>
						<div class="in collapsed" data-toggle="collapse" data-target="#collapseJawaTengah" aria-expanded="false" aria-controls="collapseJawaTengah"><i class="fas fa-angle-right"></i> Jawa Tengah</div>
						<div id="collapseJawaTengah" class="collapse" data-parent="#accordionArea">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li>
						<div class="in collapsed" data-toggle="collapse" data-target="#collapseYogyakarta" aria-expanded="false" aria-controls="collapseYogyakarta"><i class="fas fa-angle-right"></i> DI Yogyakarta</div>
						<div id="collapseYogyakarta" class="collapse" data-parent="#accordionArea">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
				</ul>
    		</div> -->
    		<div class="tab-content tab-map">
				<div class="tab-pane fade show active" id="banten" role="tabpanel" aria-labelledby="banten-tab">
					<img src="{{asset('images/map.jpg')}}" alt="" title=""/>
				</div>
				<div class="tab-pane fade" id="jakarta" role="tabpanel" aria-labelledby="jakarta-tab">
					<img src="{{asset('images/map.jpg')}}" alt="" title=""/>
				</div>
				<div class="tab-pane fade" id="jawabarat" role="tabpanel" aria-labelledby="jawabarat-tab">
					<img src="{{asset('images/map.jpg')}}" alt="" title=""/>
				</div>
				<div class="tab-pane fade" id="jawatengah" role="tabpanel" aria-labelledby="jawatengah-tab">
					<img src="{{asset('images/map.jpg')}}" alt="" title=""/>
				</div>
				<div class="tab-pane fade" id="yogyakarta" role="tabpanel" aria-labelledby="yogyakarta-tab">
					<img src="{{asset('images/map.jpg')}}" alt="" title=""/>
				</div>
			</div>
			<div class="container">
				<ul class="nav nav-tabs nav-map" role="tablist">
					<li class="nav-item">
						<div class="nav-link active" id="banten-tab" data-toggle="tab" href="#banten" role="tab" aria-controls="banten" aria-selected="true">
							<div class="t-map"><i class="fas fa-angle-right"></i> <i class="fas fa-angle-down"></i> Banten</div>
						</div>
						<div class="box-collapsed">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="nav-link" id="jakarta-tab" data-toggle="tab" href="#jakarta" role="tab" aria-controls="jakarta" aria-selected="false">
							<div class="t-map"><i class="fas fa-angle-right"></i> <i class="fas fa-angle-down"></i> Jakarta</div>
						</div>
						<div class="box-collapsed">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="nav-link" id="jawabarat-tab" data-toggle="tab" href="#jawabarat" role="tab" aria-controls="jawabarat" aria-selected="false">
							<div class="t-map"><i class="fas fa-angle-right"></i> <i class="fas fa-angle-down"></i> Jawa Barat</div>
						</div>
						<div class="box-collapsed">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="nav-link" id="jawatengah-tab" data-toggle="tab" href="#jawatengah" role="tab" aria-controls="jawatengah" aria-selected="false">
							<div class="t-map"><i class="fas fa-angle-right"></i> <i class="fas fa-angle-down"></i> Jawa Tengah</div>
						</div>
						<div class="box-collapsed">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="nav-link" id="yogyakarta-tab" data-toggle="tab" href="#yogyakarta" role="tab" aria-controls="yogyakarta" aria-selected="false">
							<div class="t-map"><i class="fas fa-angle-right"></i> <i class="fas fa-angle-down"></i> DI Yogyakarta</div>
						</div>
						<div class="box-collapsed">
							<div class="t">Chat Kami</div>
							<div class="wa">
								<a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><div class="bdr"><i class="fab fa-whatsapp"></i> Whatsapp</div></a>
							</div>
						</div>
					</li>
				</ul>
			</div>
	    </div>
	    <div class="pad-program pt20">
    		<div class="container">
	    		<div class="t-program">Kolaborasi Kelas BISINDO</div>
	    		<div class="row justify-content-center">
	    			<div class="col-lg-10">
			    		<div class="bdy-program mb20">
			    			<p>PUSBISINDO telah menjalin berbagai kolaborasi baik dalam bentuk workshop dan kelas BISINDO dalam rangka sosialisasi tentang bahasa isyarat dengan para pihak di bawah ini:</p>
			    		</div>
			    	</div>
			    </div>
	    	</div>
	    	<div class="bg-partner">
	    		<div class="top-shadow"></div>
	    		<div class="container">
	    			<div class="slider-partner">
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/gojek.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/kementerian.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/jalin-mimpi.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/grab.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/ui.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/gojek.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/kementerian.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/jalin-mimpi.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/grab.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    				<div class="item">
	    					<div class="img">
		    					<img src="{{asset('images/ui.png?v.1')}}" alt="" title=""/>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="bottom-shadow"></div>
	    	</div>
	    </div>
	</section>

	<div class="bg-answer">
		<div class="img-answer"><img src="{{asset('images/background-surya.jpg')}}" alt="" title=""/></div>
		<div class="abs">
			<div class="container">
				<div class="row">
					<div class="col-md-8 offset-md-4 col-xl-7 offset-xl-5">
						<div class="slider-answer">
							<div class="item">
								<div class="t">Pertanyaan yang paling sering diajukan:</div>
								<div class="mb15">
									<div class="t-answer">Apakah Bahasa Isyarat setiap daerah di Indonesia sama?</div>
									<div class="bdy-answer">
										<p>Bahasa Isyarat setiap daerah di Indonesia berbeda-beda. Karena BISINDO merupakan Bahasa Alamiah yang muncul dan dikembangkan oleh masyarakat Tuli sesuai dengan ciri khas daerah tersebut.</p>
									</div>
								</div>
								<div class="link">
			    					<a href="{{ URL::to('/faq') }}"><button type="button" class="hvr-button">Baca lebih lanjut</button></a>
			    				</div>
							</div>
							<div class="item">
								<div class="t">Pertanyaan yang paling sering diajukan:</div>
								<div class="mb15">
									<div class="t-answer">Siapa saja bisa belajar Bisindo?</div>
									<div class="bdy-answer">
										<p>Bahasa Isyarat setiap daerah di Indonesia berbeda-beda. Karena BISINDO merupakan Bahasa Alamiah yang muncul dan dikembangkan oleh masyarakat Tuli sesuai dengan ciri khas daerah tersebut.</p>
									</div>
								</div>
								<div class="link">
			    					<a href="{{ URL::to('/faq') }}"><button type="button" class="hvr-button">Baca lebih lanjut</button></a>
			    				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-tentang">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<div class="img">
		    			<img src="{{asset('images/team.png')}}" alt="" title=""/>
		    		</div>
				</div>
				<div class="col-lg-7">
					<div class="pl30">
						<div class="t">Latar Belakang berdirinya PUSBISINDO</div>
						<div class="bdy">
							<p>semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar. semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.semua orang termasuk anak dengar, orang tua, anak tuli, guru SLB, terapis, psikolog, para profesional dan siapapun dari berbagai lapisan sosial atau semua kalangan dapat belajar.</p>
						</div>
						<div class="link">
	    					<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Baca lebih lanjut</button></a>
	    				</div>
	    			</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pad-program pb20">
		<div class="container">
			<div class="bg-follow">
				<div class="t-program">Follow Us:</div>
				<ul class="l-soc">
	                <li><a href="https://twitter.com/pusbisindo" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-twitter"></i></span></a></li>
	                <li><a href="https://www.instagram.com/pusbisindo/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-instagram"></i></span></a></li>
	                <li><a href="https://www.facebook.com/Pusbisindo/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-facebook-f"></i></span></a></li>
	            </ul>
	        </div>
			<script src="https://apps.elfsight.com/p/platform.js" defer></script>
			<div class="elfsight-app-8566bb67-75b4-47b3-b185-054fcc6f9bb2"></div>
		</div>
	</div>

	<section id="kontak-kami" data-anchor="kontak-kami">
		<div class="pad-program">
			<div class="container">
				<div class="t-program">Kontak Kami</div>
	    		<div class="bdy-program">
	    			<p>Apabila anda punya pertanyaan mengenai pelatihan, kolaborasi, permintaan narasumber/wawancara.</p>
	    			<p>Dapat menghubungi/mengirimkan email ke kami</p>
	    		</div>
	    		<div class="bg-tentang-kami">
		    		<div class="row">
		    			<div class="col-md-6 col-lg-5 order-2 order-md-1">
		    				<div class="mb30">
			    				<div class="t-kami">Alamat Surat</div>
			    				<div class="bdy-kami">
			    					<p class="mb15">Komplek Depkes, Jl H. Umaidi No. Bambu 2, Rt. 10/07, Jl. Raya Pasar Minggu No.39 A, RT.1/RW.7, Rawa Barat, Kec. Pasar. Minggu, Jakarta Selatan - DKI Jakarta 12510 Indonesia</p>
			    					<p><span class="bold">Catatan:</span> Bagi yang ingin datang, diharapkan menghubungi / bikin janji terlebih dahulu melalui email / whatsapp.</p>
			    				</div>
			    			</div>
			    			<div>
			    				<div class="t-kami">Informasi Kontak</div>
			    				<div class="bdy-kami">
			    					<p class="mb15">Email: <a href="mailto:pusbisindo@gmail.com">pusbisindo@gmail.com</a></p>
			    					<p class="mb15">Whatsapp (Hanya Chat): <a href="#">081280002902</a></p>
									<p>Jam Kerja:</p>
									<p>Senin - Jumat: 08.00 - 17.00</p>
									<p>Sabtu: 09.00 - 12.00</p>
									<p>Minggu: Libur</p>
			    				</div>
			    			</div>
		    			</div>
		    			<div class="col-md-6 col-lg-7 order-1 order-md-2">
		    				<div class="img">
				    			<img src="{{asset('images/tentang-kami.jpg')}}" alt="" title=""/>
				    		</div>
		    			</div>
		    		</div>
		    	</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<script type="text/javascript">
    $(function() {
    	$('.slider-banner').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: true,
            autoplay: true,
			fade: false,
			adaptiveHeight: true
		});

		$('.slider-program').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
            autoplay: false,
			fade: false,
			adaptiveHeight: true
		});

		// $(".collapse.show").each(function(){
	 //        $(this).prev("ul.accordionArea li .in").find(".fas").addClass("fa-angle-down").removeClass("fa-angle-right");
	 //    });

	 //    $(".collapse").on('show.bs.collapse', function(){
	 //        $(this).prev("ul.accordionArea li .in").find(".fas").removeClass("fa-angle-right").addClass("fa-angle-down");
	 //    }).on('hide.bs.collapse', function(){
	 //        $(this).prev("ul.accordionArea li .in").find(".fas").removeClass("fa-angle-down").addClass("fa-angle-right");
	 //    });

	    $('.slider-partner').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: true,
			responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			}
			]
		});

		$('.slider-answer').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
            autoplay: false,
			fade: false,
			adaptiveHeight: true
		});
		$( "ul.nav-map li:first-child" ).addClass('active');
		
		$('ul.nav-map li .nav-link').click(function(event) {
			$('ul.nav-map li').removeClass('active');
			$(this).parent().addClass('active');
		});
    });
</script>
@endsection
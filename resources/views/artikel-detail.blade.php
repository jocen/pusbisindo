@extends('layout')

@section('content')
	<div class="css-artikel">
		<div class="pad-program">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-8 xs30">
								<div class="detail">
									<div class="t-detail">Etika Berkomunikasi dengan Tuli</div>
                                    <div class="date-detail">12 Oktober 2020 | sumber: </div>
                                    <div class="img-detail">
                                    	<img src="{{asset('images/artikel.jpg?v.1')}}" alt="" title=""/>
                                	</div>
									<div class="bdy-detail">
										<p>Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman</p><br/>
										<p>Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan</p><br/>
										<p>program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman  Tersedia pilihan program kelas bahasa isyarat  bisa diambil sesuai dengan kebutuhan anda Semua program kelas didampingi para guru Tuli yang berpengalaman</p>
									</div>
									<div class="fb-comments" data-href="http://www.pusbisindo.org/artikel-detail" data-numposts="10" data-width=""></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="box-artikel">
									<div class="t-artikel2">Pencarian</div>
									<div class="input-artikel fullwidth">
										<input type="text" class="form-control" placeholder="Cari..." />
										<button type="submit"><img src="{{asset('images/search2.png?v.1')}}" alt="" title=""/></button>
									</div>
									<div class="t-artikel2 big">Kategori</div>
									<ul class="l-artikel">
										<li><a href="{{ URL::to('/artikel-detail') }}">Budaya Tuli</a></li>
										<li><a href="{{ URL::to('/artikel-detail') }}">Dunia Tuli</a></li>
									</ul>
									<div class="t-artikel2 small">Popular Post</div>
									<div class="item-artikel2 small">
										<a href="{{ URL::to('/artikel-detail') }}">
											<div class="tbl">
												<div class="cell img-artikel">
													<img src="{{asset('images/artikel.jpg?v.1')}}" alt="" title=""/>
												</div>
												<div class="cell">
													<div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
												</div>
											</div>
										</a>
									</div>
									<div class="item-artikel2 small">
										<a href="{{ URL::to('/artikel-detail') }}">
											<div class="tbl">
												<div class="cell img-artikel">
													<img src="{{asset('images/artikel.jpg?v.1')}}" alt="" title=""/>
												</div>
												<div class="cell">
													<div class="t">Etika Berkomunikasi dengan Tuli</div>
		                                            <div class="date">12 Oktober 2020 | sumber: </div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
			    	</div>
			    </div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>

<script type="text/javascript">
	function widthMaxbdy(){
		if ($(window).width() > 1024) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 120) + '...');
		    });
		}
		else if ($(window).width() > 992) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 100) + '...');
		    });
		}
		else if ($(window).width() > 767) {
			$('.item-artikel a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 80) + '...');
		    });
		}
	}

	function widthMaxbdy2(){
		if ($(window).width() > 1024) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 140) + '...');
		    });
		}
		else if ($(window).width() > 992) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 80) + '...');
		    });
		}
		else if ($(window).width() > 768) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 60) + '...');
		    });
		}
		else if ($(window).width() < 767) {
			$('.item-artikel2 a .bdy p').each(function() {
		        $(this).text($(this).text().substr(0, 40) + '...');
		    });
		}
	}
    $(function() {
    	$('.nav-artikel').addClass('active');

    	$('.slider-banner').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
            autoplay: true,
			fade: false,
			adaptiveHeight: true
		});

	    widthMaxbdy();
	    widthMaxbdy2();
		
		$(window).on('resize', function(){
			widthMaxbdy();
			widthMaxbdy2();
		});
    });
</script>
@endsection
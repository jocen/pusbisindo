<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0"/>
    <meta name="description" content="Pusat Bahasa Isyarat Indonesia">
    <meta name="keywords" content="Pusat Bahasa Isyarat Indonesia">
    <title>PUSBISINDO | Pusat Bahasa Isyarat Indonesia</title>
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon180x180.jpg')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon16x16.jpg')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon32x32.jpg')}}">
    
    <!-- CSS -->
    @yield('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/front.css?v.4') }}" rel="stylesheet"/>

</head>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v9.0&appId=1409463772627039&autoLogAppEvents=1" nonce="X17Y12KF"></script>

    <h1 style="display: none;">Pusbisindo</h1>
    <header>
        <div class="header-top">
            <div class="container">
                <ul class="l-top">
                    <li>Translate : </li>
                    <li>
                        <a href="http://translate.google.com/translate?hl=en&sl=en&tl=en&u=https%3A%2F%2Fwww.pusbisindo.org%2F" target="_blank" rel="noreferrer noopener">English</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-7 col-md-3 my-auto">
                        <div class="logo">
                            <a href="{{ URL::to('/') }}">
                                <img src="{{asset('images/logo.png?v.1')}}" alt="" title=""/>
                            </a>
                        </div>
                    </div>
                    <div class="col-5 col-md-9 my-auto text-right">
                        <div class="menu">
                            <i class="fas fa-bars"></i>
                        </div>
                        <div class="main-menu hidden-xs">
                            <nav>
                                <a href="{{ URL::to('/') }}" data-scroll="beranda">Beranda</a>
                                <a href="{{ URL::to('/#program') }}" data-scroll="program">Program</a>
                                <a href="{{ URL::to('/#kelas-area') }}" data-scroll="area-kelas">Area Kelas</a>
                                <a href="{{ URL::to('/artikel') }}" class="nav-artikel">Artikel</a>
                                <a href="{{ URL::to('/faq') }}" class="nav-faq">FAQ</a>
                                <a href="{{ URL::to('/tentang-kami') }}" class="nav-tentang-kami">Tentang Kami</a>
                                <a href="{{ URL::to('/#kontak-kami') }}" data-scroll="kontak-kami">Kontak Kami</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main">
        @yield('content')
    </div>

    <footer class="getMenu" id="kontak">
        <div class="container">
            <div class="row">
                <div class="col-md-5 xs30">
                    <div class="mb40">
                        <div class="t-footer">Program Kelas Bahasa Isyarat</div>
                        <ul class="l-footer">
                            <li><a href="#">Kelas Regular</a></li>
                            <li><a href="#">Kelas Privat</a></li>
                        </ul>
                    </div>
                    <div class="t-footer">Tentang Kami</div>
                    <ul class="l-footer">
                        <li><a href="{{ URL::to('tentang-kami') }}">Latar Belakang</a></li>
                        <li><a href="{{ URL::to('tentang-kami#timeline') }}">Timeline</a></li>
                        <li><a href="{{ URL::to('tentang-kami#kepengurusan-pusat') }}">Kepengurusan Pusat</a></li>
                        <li><a href="{{ URL::to('tentang-kami#kepengurusan-cabang') }}">Kepengurusan Cabang</a></li>
                    </ul>
                </div>
                <div class="col-md-4 xs30">
                    <div class="t-footer">Pusat Informasi</div>
                    <ul class="l-footer">
                        <li><a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fas fa-phone-alt"></i></span> +6281280002902 (Hanya Chat)</a></li>
                        <li><a href="mailto:pusbisindo@gmail.com"><span class="icon"><i class="fas fa-envelope"></i></span> pusbisindo@gmail.com</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="t-footer">Social Media</div>
                    <ul class="l-soc">
                        <li><a href="https://twitter.com/pusbisindo" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-twitter"></i></span></a></li>
                        <li><a href="https://www.instagram.com/pusbisindo/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-instagram"></i></span></a></li>
                        <li><a href="https://www.facebook.com/Pusbisindo/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-facebook-f"></i></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="cp">Copyright &copy; <a href="https://api.whatsapp.com/send?phone=628977707814" target="_blank">stefanstar</a> <?php echo date("Y"); ?></div>
        </div>
    </footer>

    <div class="fixed-top">
        <a>
            <div class="tbl">
                <div class="cell">
                    <div class="text">
                        <i class="fas fa-chevron-up"></i>
                        <div>TOP</div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="fixed-message">
        <a href="https://wa.me/message/F6ZDXVKVANBDD1" target="_blank" rel="noreferrer noopener">
            <div class="tbl">
                <div class="cell">
                    <div class="txt">
                        <div class="lt">Info & Pertanyaan</div>
                        <div class="bold">HUBUNGI KAMI</div>
                    </div>
                    <div class="img">
                        <img src="{{asset('images/chat.png?v.1')}}"/>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="overlay"></div>
    <div class="slide-menu">
        <div class="box-close">
            <i class="fas fa-times"></i>
        </div>
        <div class="pad20">
           <nav>
                <a href="{{ URL::to('/') }}" data-scroll="beranda" class="active">Beranda</a>   
                <a href="{{ URL::to('/#program') }}" data-scroll="program">Program</a>
                <a href="{{ URL::to('/#kelas-area') }}" data-scroll="area-kelas">Area Kelas</a>
                <a href="{{ URL::to('/artikel') }}" data-scroll="artikel">Artikel</a>
                <a href="{{ URL::to('/faq') }}" data-scroll="faq">FAQ</a>
                <a href="{{ URL::to('/tentang-kami') }}" data-scroll="tentang-kami">Tentang Kami</a>
                <a href="{{ URL::to('/#kontak-kami') }}" data-scroll="kontak-kami">Kontak Kami</a>
            </nav>
        </div>
    </div>


<script type="text/javascript" src="{{ asset('jquery-3.5.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('web.js') }}"></script>

<!-- JS -->
@yield('js')

</body>
</html>
        
